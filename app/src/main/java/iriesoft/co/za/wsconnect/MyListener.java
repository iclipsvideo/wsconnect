package iriesoft.co.za.wsconnect;

public class MyListener {

    public MyEnums.EventType eventType;
    public Exception exception;
    public String echo;

    private ChangeListener listener;

    public void setEcho(MyEnums.EventType e) {
        this.eventType = e;
        if (listener != null) listener.onChange();
    }

    public void setEcho(MyEnums.EventType e, String s) {
        this.eventType = e;
        this.echo = s;
        if (listener != null) listener.onChange();
    }

    public void setEcho(MyEnums.EventType e, Exception ex) {
        this.eventType = e;
        this.exception = ex;
        if (listener != null) listener.onChange();
    }

    public ChangeListener getListener() {
        return listener;
    }

    public void setListener(ChangeListener listener) {
        this.listener = listener;
    }

    public interface ChangeListener {
        void onChange();
    }
}
