package iriesoft.co.za.wsconnect;

import com.google.gson.Gson;

public class MyMessage {

    public String action;
    public String message;
    public String username;

    public MyMessage(String _action, String _message, String _username) {
        action = _action;
        message = _message;
        username = _username;
    }

    public MyMessage() {
    }

    public String toString() {
        return new Gson().toJson(this).toString();
    }

}