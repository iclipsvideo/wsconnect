package iriesoft.co.za.wsconnect;

import android.app.Activity;
import android.widget.Toast;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class MyClient {

    private static Activity activity;
    WebSocket webSocket;

    public MyClient(Activity a, final MyListener myListener, String webSocketUrl, int timeout) throws IOException {
        activity = a;

        webSocket = new WebSocketFactory().setConnectionTimeout(timeout).createSocket(webSocketUrl)

                .addListener(new WebSocketAdapter() {
                    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {

                        myListener.setEcho(MyEnums.EventType.CONNECT);

                    }
                })

                .addListener(new WebSocketAdapter() {

                    public void onTextMessage(WebSocket websocket, String message) {

                        myListener.setEcho(MyEnums.EventType.MESSAGE, message);

                    }
                }).addListener(new WebSocketAdapter() {

                    public void onConnectError(WebSocket websocket, WebSocketException exception) {

                        myListener.setEcho(MyEnums.EventType.CONNECTERROR, exception);

                    }
                }).addListener(new WebSocketAdapter() {

                    public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame,
                                               WebSocketFrame clientCloseFrame, boolean closedByServer) {

                        myListener.setEcho(MyEnums.EventType.DISCONNECT);

                    }
                })

                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE);
    }

    private void output(final String s) {
        activity.runOnUiThread((new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity.getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }));
    }
}