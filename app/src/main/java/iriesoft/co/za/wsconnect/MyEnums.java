package iriesoft.co.za.wsconnect;

public class MyEnums {
    public static enum EventType {
        CONNECT,
        CONNECTERROR,
        DISCONNECT,
        MESSAGE
    }
}
