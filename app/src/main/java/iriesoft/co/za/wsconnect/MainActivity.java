package iriesoft.co.za.wsconnect;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    // ui controls
    @BindView(R.id.lblMessage) TextView lblMessage;
    @BindView(R.id.btnAction) TextView btnAction;

    // variables
    String url = "wss://ws-server-cixyauylpn.now.sh";
    int timeout = 5000;
    MyClient myClient;
    MyListener myListener;

    // method for displaying toasts
    private void t(final String s){
        runOnUiThread((new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }));
    }

    // method for textview output
    private void l(final String s){
        runOnUiThread((new Runnable() {
            @Override
            public void run() {
                lblMessage.append("\r\n");
                lblMessage.append(s);
            }
        }));
    }

    @OnClick(R.id.btnAction) void click() {

        if (btnAction.getText().equals("CONNECT")) {
            // create a client
            try {
                btnAction.setText("CONNECTING...");
                t("LOG: " + "Connecting to server...");
                l("LOG: " + "Connecting to server...");
                myClient = new MyClient(MainActivity.this, myListener, url, timeout);
            } catch (IOException e) {
                t("Could not initialize web socket connection. " + e.getMessage());
                l("ERROR: " + "Could not initialize web socket connection. " + e.getMessage());
            }
            // call connect on our client
            myClient.webSocket.connectAsynchronously();

        } else {
            if (myClient.webSocket.isOpen()) {
                myClient.webSocket.disconnect();
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bind ui controls
        ButterKnife.bind(this);

        // Initialize variables
        myListener = new MyListener();

        // Initialize controls
        lblMessage.setText("Press button to connect to server");
        btnAction.setText("CONNECT");

        // Set up listening class
        myListener.setListener(new MyListener.ChangeListener() {
            @Override
            public void onChange() {

                switch (myListener.eventType)
                {
                    case CONNECT:

                        runOnUiThread((new Runnable() {
                            @Override
                            public void run() {
                                btnAction.setText("DISCONNECT");
                                t("Connected to Web Socket");
                                l("LOG: " + "Connected to Web Socket");

                                if(myClient.webSocket.isOpen())
                                {
                                    DateFormat df = new SimpleDateFormat("HHmmss");
                                    Date today = Calendar.getInstance().getTime();
                                    String date = df.format(today);

                                    MyMessage m = new MyMessage();
                                    m.action = "add user";
                                    m.message = "hello";
                                    m.username = "abc" + date;

                                    l("ME: " + m.toString());
                                    myClient.webSocket.sendText(m.toString());
                                }

                            }
                        }));

                        break;
                    case CONNECTERROR:
                        t("Error connecting to Web Socket. " + myListener.exception.getMessage());
                        l("ERROR: " + "Error connecting to Web Socket. " + myListener.exception.getMessage());
                        break;
                    case DISCONNECT:

                        runOnUiThread((new Runnable() {
                            @Override
                            public void run() {
                                btnAction.setText("CONNECT");
                                lblMessage.setText("");
                                t("Disconnected from Web Socket.");
                                l("LOG: " + "Disconnected from Web Socket.");
                                l("===");
                            }
                        }));

                        break;
                    case MESSAGE:
                        t("Message from server: " + myListener.echo);
                        l("SERVER: " + myListener.echo);
                        break;
                }
            }
        });

    }
}
